/**
 * pd_cfg.c
 *
 * Copyright (c) 2023 <ch.shen@foxmail.com>
 *
 * The PD-Tricker Project. All rights reserved. Use of this source code
 * is governed by a license that can be found in the LICENSE file.
 *
 * Created on: 2023-12-09
 *     Author: sch
 **/

#include "pd_cfg.h"
#include "ADC.H"
#include "stdio.h"

sbit CFG_1 = P3 ^ 3;
sbit CFG_2 = P3 ^ 4;
sbit CFG_3 = P3 ^ 5;

void pd_cfg_mode(PD_MODE_E mode)
{
    switch (mode) {
    case PD_MODE_5V:
        CFG_1 = 1;
        CFG_2 = 0;
        CFG_3 = 0;
        break;

    case PD_MODE_9V:
        CFG_1 = 0;
        CFG_2 = 0;
        CFG_3 = 0;
        break;

    case PD_MODE_12V:
        CFG_1 = 0;
        CFG_2 = 0;
        CFG_3 = 1;
        break;

    case PD_MODE_15V:
        CFG_1 = 0;
        CFG_2 = 1;
        CFG_3 = 1;
        break;

    case PD_MODE_20V:
        CFG_1 = 0;
        CFG_2 = 1;
        CFG_3 = 0;
        break;

    default:
        break;
    }
}

BOOL pd_verify_adc(PD_MODE_E mode)
{
    BOOL res = 0;
    UINT16 vbus_voltage;

    ADC_ChannelSelect(0);

    ADC_START = 1;
    while (ADC_START);

    vbus_voltage = (UINT16)ADC_DATA;

    switch (mode) {
    case PD_MODE_5V:
        res = (vbus_voltage >= 57) && (vbus_voltage <= 69);
        break;

    case PD_MODE_9V:
        res = (vbus_voltage >= 108) && (vbus_voltage <= 120);
        break;

    case PD_MODE_12V:
        res = (vbus_voltage >= 146) && (vbus_voltage <= 158);
        break;

    case PD_MODE_15V:
        res = (vbus_voltage >= 184) && (vbus_voltage <= 197);
        break;

    case PD_MODE_20V:
        res = (vbus_voltage >= 247) && (vbus_voltage <= 260);
        break;

    default:
        break;
    }

    if (res == FALSE) {
        printf("VBUS:%d (%ld mV)\n", vbus_voltage, ((((UINT32)(vbus_voltage) * 20130)) >> 8));
    }

    return res;
}
