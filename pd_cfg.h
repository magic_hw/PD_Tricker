/**
 * pd_cfg.h
 *
 * Copyright (c) 2023 <ch.shen@foxmail.com>
 *
 * The PD-Tricker Project. All rights reserved. Use of this source code
 * is governed by a license that can be found in the LICENSE file.
 *
 * Created on: 2023-12-09
 *     Author: sch
 **/

#ifndef PD_CFG_H_
#define PD_CFG_H_

#include "CH552.H"

typedef enum {
    PD_MODE_5V = 0,
    PD_MODE_9V,
    PD_MODE_12V,
    PD_MODE_15V,
    PD_MODE_20V,
} PD_MODE_E;

void pd_cfg_mode(PD_MODE_E mode);

BOOL pd_verify_adc(PD_MODE_E mode);

#endif /* PD_CFG_H_ */
