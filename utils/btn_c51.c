/**
 * btn_c51.c
 *
 * Copyright (c) 2023 <ch.shen@foxmail.com>
 *
 * The PD-Tricker Project. All rights reserved. Use of this source code
 * is governed by a license that can be found in the LICENSE file.
 *
 * Created on: 2023-12-09
 *     Author: sch
 **/

#include "btn_c51.h"

typedef enum {
    BTN_STATE_IDLE,
    BTN_STATE_UP,
    BTN_STATE_UP_SUSPEND,
    BTN_STATE_DOWN,
    BTN_STATE_DOWN_SUSPEND,
    BTN_STATE_DOWN_LONG,
} BTN_STATE_E;

typedef enum {
    BTN_VAL_UP = 0,
    BTN_VAL_DOWN,
} BTN_VAL_E;

void button_init(button_t *p_btn, UINT16 long_min_cnt, UINT16 up_max_cnt, UINT8 active_level)
{
    if (p_btn) {
        p_btn->active_lv = active_level;
        p_btn->long_min_cnt = long_min_cnt;
        p_btn->up_max_cnt = up_max_cnt;

        p_btn->jit.asserted = (active_level ? 0 : 1);
        p_btn->jit.previous = p_btn->jit.asserted;
        p_btn->counter = 0;
        p_btn->state = BTN_STATE_IDLE;
    }
}

BTN_EVENT_E button_scan(button_t *p_btn, UINT8 btn_val)
{
    BTN_EVENT_E ev = BTN_EVENT_NONE;
    UINT8 cur_btn_val;

    if (!p_btn) {
        return BTN_EVENT_NONE;
    }

    /* 软件去抖 */
    p_btn->jit.asserted |= (p_btn->jit.previous & btn_val);
    p_btn->jit.asserted &= (p_btn->jit.previous | btn_val);
    p_btn->jit.previous = btn_val;

    cur_btn_val = (p_btn->jit.asserted == p_btn->active_lv) ? BTN_VAL_DOWN : BTN_VAL_UP;

    switch (p_btn->state) {
    case BTN_STATE_IDLE:
        if (cur_btn_val) {
            p_btn->state = BTN_STATE_DOWN;

            p_btn->counter = 0;
            p_btn->click_cnt = 0;
        } else {
            p_btn->state = BTN_STATE_UP;
        }
        break;

    case BTN_STATE_UP:
        if (cur_btn_val) {
            p_btn->state = BTN_STATE_DOWN;

            p_btn->counter = 0;
            p_btn->click_cnt = 0;
        } else {
            /* nothing to do */
        }
        break;

    case BTN_STATE_UP_SUSPEND:
        if (cur_btn_val) {
            p_btn->counter = 0;
            ++p_btn->click_cnt;
            p_btn->state = BTN_STATE_DOWN_SUSPEND;
        } else {
            if (++p_btn->counter >= p_btn->up_max_cnt) {
                p_btn->counter = 0;
                p_btn->state = BTN_STATE_UP;

                if (p_btn->click_cnt > 1) {
                    /* 多击也只认作双击 */
                    ev = BTN_EVENT_DOUBLE_CLICK;
                } else {
                    ev = BTN_EVENT_SINGLE_CLICK;
                }
            }
        }
        break;

    case BTN_STATE_DOWN:
        if (cur_btn_val) {
            if (++p_btn->counter >= p_btn->long_min_cnt) {
                p_btn->counter = 0;
                p_btn->state = BTN_STATE_DOWN_LONG;

                ev = BTN_EVENT_LONG_PRESS;
            }
        } else {
            p_btn->counter = 0;
            ++p_btn->click_cnt;
            p_btn->state = BTN_STATE_UP_SUSPEND;

            ev = BTN_EVENT_UP;
        }
        break;

    case BTN_STATE_DOWN_SUSPEND:
        if (cur_btn_val) {
            if (++p_btn->counter >= p_btn->up_max_cnt) {
                /* 大于两种不同电平间隔,此时按下次数必然大于 1 */
                p_btn->counter = 0;
                ev = BTN_EVENT_DOUBLE_CLICK;

                p_btn->state = BTN_STATE_DOWN_LONG;
            }
        } else {
            p_btn->counter = 0;
            p_btn->state = BTN_STATE_UP_SUSPEND;

            ev = BTN_EVENT_UP;
        }

        break;

    case BTN_STATE_DOWN_LONG:
        if (cur_btn_val) {
            /* Do nothing */
        } else {
            p_btn->state = BTN_STATE_UP;

            ev = BTN_EVENT_UP;
        }
        break;
    }

    return ev;
}
