/**
 * btn_c51.h
 *
 * Copyright (c) 2023 <ch.shen@foxmail.com>
 *
 * The PD-Tricker Project. All rights reserved. Use of this source code
 * is governed by a license that can be found in the LICENSE file.
 *
 * Created on: 2023-12-09
 *     Author: sch
 **/

#ifndef BTN_C51_H_
#define BTN_C51_H_

#include "btn_cfg.h"

typedef enum {
    BTN_EVENT_NONE,
    BTN_EVENT_UP,
    BTN_EVENT_SINGLE_CLICK,
    BTN_EVENT_DOUBLE_CLICK,
    BTN_EVENT_LONG_PRESS,
} BTN_EVENT_E;

typedef struct {
    UINT8 previous;
    UINT8 asserted;
} button_jitter_t;

typedef struct {
    UINT8 active_lv;
    button_jitter_t jit;
    UINT8 click_cnt;
    UINT8 state;
    UINT16 long_min_cnt;
    UINT16 up_max_cnt;
    UINT16 counter;
} button_t;

void button_init(button_t *p_btn, UINT16 long_min_cnt, UINT16 up_max_cnt, UINT8 active_level);

BTN_EVENT_E button_scan(button_t *p_btn, UINT8 btn_val);

#endif /* BTN_C51_H_ */
