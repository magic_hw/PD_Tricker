/**
 * leds.h
 *
 * Copyright (c) 2023 <ch.shen@foxmail.com>
 *
 * The PD-Tricker Project. All rights reserved. Use of this source code
 * is governed by a license that can be found in the LICENSE file.
 *
 * Created on: 2023-12-09
 *     Author: sch
 **/

#ifndef LEDS_H_
#define LEDS_H_

#include "CH552.H"

typedef enum {
    LED_MODE_OFF,
    LED_MODE_ON,
    LED_MODE_BLINKY_SLOW,
    LED_MODE_BLINKY_FAST,
} LED_MODE_E;

typedef enum {
    LED_TYPE_5V = 0,
    LED_TYPE_9V,
    LED_TYPE_12V,
    LED_TYPE_15V,
    LED_TYPE_20V,
} LED_TYPE_E;

void leds_mode_set(LED_MODE_E mode);

void leds_type_select(LED_TYPE_E type);

void leds_process(void);

#endif /* LEDS_H_ */
