/**
 * leds.c
 *
 * Copyright (c) 2023 <ch.shen@foxmail.com>
 *
 * The PD-Tricker Project. All rights reserved. Use of this source code
 * is governed by a license that can be found in the LICENSE file.
 *
 * Created on: 2023-12-09
 *     Author: sch
 **/

#include "leds.h"

typedef struct {
    UINT8 index;
    UINT8 num;
    UINT16 cnt;
    UINT16 *seq;
} blinky_t;

sbit LED_5V  = P3 ^ 2;
sbit LED_9V  = P1 ^ 4;
sbit LED_12V = P1 ^ 5;
sbit LED_15V = P1 ^ 6;
sbit LED_20V = P1 ^ 7;

static volatile UINT8 m_cur_led_select = LED_TYPE_5V;
static UINT8 m_prev_led_select = 0x5A;

static volatile UINT8 m_led_mode = LED_MODE_BLINKY_SLOW;
static UINT8 m_prev_led_mode = 0x5A;

/* unit in 10ms */
static UINT16 m_blinky_slow_seq[] = { 25, 25 };
static UINT16 m_blinky_fast_seq[] = { 10, 10, 10, 10, 10, 50, };

static blinky_t m_blinky;

static void leds_all_off(void)
{
    LED_5V = 0;
    LED_9V = 0;
    LED_12V = 0;
    LED_15V = 0;
    LED_20V = 0;
}

static void led_on(LED_TYPE_E type)
{
    switch (type) {
    case LED_TYPE_5V:
        LED_5V = 1;
        break;

    case LED_TYPE_9V:
        LED_9V = 1;
        break;

    case LED_TYPE_12V:
        LED_12V = 1;
        break;

    case LED_TYPE_15V:
        LED_15V = 1;
        break;

    case LED_TYPE_20V:
        LED_20V = 1;
        break;
    }
}

static void led_toggle(LED_TYPE_E type)
{
    switch (type) {
    case LED_TYPE_5V:
        LED_5V = !LED_5V;
        break;

    case LED_TYPE_9V:
        LED_9V = !LED_9V;
        break;

    case LED_TYPE_12V:
        LED_12V = !LED_12V;
        break;

    case LED_TYPE_15V:
        LED_15V = !LED_15V;
        break;

    case LED_TYPE_20V:
        LED_20V = !LED_20V;
        break;
    }
}

void leds_mode_set(LED_MODE_E mode)
{
    if (mode <= LED_MODE_BLINKY_FAST) {
        m_led_mode = mode;
    }
}

void leds_type_select(LED_TYPE_E type)
{
    if (type <= LED_TYPE_20V) {
        m_cur_led_select = type;
    }
}

void leds_process(void)
{
    if ((m_prev_led_select != m_cur_led_select) || (m_prev_led_mode != m_led_mode)) {
        m_prev_led_select = m_cur_led_select;
        m_prev_led_mode = m_led_mode;

        leds_all_off();

        switch (m_led_mode) {
        case LED_MODE_OFF:
            /* 已经关闭，无需再做什么 */
            break;

        case LED_MODE_ON:
            led_on(m_cur_led_select);
            break;

        case LED_MODE_BLINKY_SLOW:
            m_blinky.cnt = 0;
            m_blinky.index = 0;
            m_blinky.num = sizeof(m_blinky_slow_seq) / sizeof(m_blinky_slow_seq[0]);
            m_blinky.seq = m_blinky_slow_seq;
            break;

        case LED_MODE_BLINKY_FAST:
            m_blinky.cnt = 0;
            m_blinky.index = 0;
            m_blinky.num = sizeof(m_blinky_fast_seq) / sizeof(m_blinky_fast_seq[0]);
            m_blinky.seq = m_blinky_fast_seq;
            break;
        }
    }

    if ((m_led_mode == LED_MODE_BLINKY_SLOW) || (m_led_mode == LED_MODE_BLINKY_FAST)) {
        if (m_blinky.cnt == 0) {
            led_toggle(m_cur_led_select);
        }

        if (++m_blinky.cnt >= m_blinky.seq[m_blinky.index]) {
            m_blinky.cnt = 0;
            ++m_blinky.index;
            if (m_blinky.index >= m_blinky.num) {
                m_blinky.index = 0;
            }
        }
    }
}
