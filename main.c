/**
 * main.c
 *
 * Copyright (c) 2023 <ch.shen@foxmail.com>
 *
 * The PD-Tricker Project. All rights reserved. Use of this source code
 * is governed by a license that can be found in the LICENSE file.
 *
 * Created on: 2023-12-09
 *     Author: sch
 **/

#include "stdio.h"
#include "CH552.H"

#include "ADC.H"
#include "GPIO.H"
#include "DEBUG.H"

#include "leds.h"
#include "btn_c51.h"
#include "pd_cfg.h"

/* 主循环周期 */
#define PERIOD_MS           (10)

/* 按键参数 */
#define BTN_LONG_PRESS_MS   (2500)
#define BTN_UP_MAX_MS       (300)

#define BTN_ACTIVE_LEVEL    (0)

#define BTN_PCNT(m)         ((m) / PERIOD_MS)

#define VERIFY_MAX_CNT      (20)

typedef enum {
    SYS_STATE_SELECT,
    SYS_STATE_VERIFYING,
    SYS_STATE_VERIFY_OK,
    SYS_STATE_VERIFY_FAIL,
} SYS_STATE_E;

sbit KEY = P1 ^ 0;

static volatile SYS_STATE_E m_sys_state = SYS_STATE_SELECT;
static UINT16 m_verify_cnt = 0;

static button_t m_btn;

static UINT8 m_cur_pd = PD_MODE_5V;

/* *IDENT-OFF* */
/* 配置失败时强制 PD-5V */
#define DO_PD_CFG_FAIL()                        \
    do {                                        \
        pd_cfg_mode(PD_MODE_5V);                \
        leds_mode_set(LED_MODE_BLINKY_FAST);    \
        m_sys_state = SYS_STATE_VERIFY_FAIL;    \
    } while(0);

/* 选择模式下切换到 PD-5V */
#define SWITCH_TO_PD_SELECT()                   \
    do {                                        \
        pd_cfg_mode(PD_MODE_5V);                \
        leds_mode_set(LED_MODE_BLINKY_SLOW);    \
        m_sys_state = SYS_STATE_SELECT;         \
    } while(0);
/* *IDENT-ON* */

/* TX:  P3.1/PWM2/TXD */
/* RX:  P3.0/PWM1/RXD */
/* uart-0 baudrate: 57600 */
void main()
{
    BTN_EVENT_E btn_ev;

    CfgFsys();

    Port1Cfg(0, 0);
    pd_cfg_mode(m_cur_pd);
    leds_mode_set(LED_MODE_BLINKY_SLOW);
    leds_type_select(m_cur_pd);

    mInitSTDIO();

    ADCInit(0);

    button_init(&m_btn, BTN_PCNT(BTN_LONG_PRESS_MS), BTN_PCNT(BTN_UP_MAX_MS), BTN_ACTIVE_LEVEL);

    printf("\r\nPDTricker build [" __TIME__ " " __DATE__ "] \r\n");

    while ( 1 ) {
        btn_ev = button_scan(&m_btn, KEY);

        switch (m_sys_state) {
        case SYS_STATE_SELECT:
            if (btn_ev == BTN_EVENT_SINGLE_CLICK) {
                ++m_cur_pd;
                if (m_cur_pd > LED_TYPE_20V) {
                    m_cur_pd = LED_TYPE_5V;
                }
                leds_type_select(m_cur_pd);
            } else if (btn_ev == BTN_EVENT_DOUBLE_CLICK) {
                pd_cfg_mode((PD_MODE_E) m_cur_pd);

                m_sys_state = SYS_STATE_VERIFYING;
                m_verify_cnt = 0;
            }
            break;

        case SYS_STATE_VERIFYING:
            if (pd_verify_adc(m_cur_pd)) {
                leds_mode_set(LED_MODE_ON);

                m_sys_state = SYS_STATE_VERIFY_OK;
                printf("VERIFY %d\n", m_verify_cnt);
            } else {
                ++m_verify_cnt;
                if (m_verify_cnt >= VERIFY_MAX_CNT) {
                    DO_PD_CFG_FAIL();
                }
            }
            break;

        case SYS_STATE_VERIFY_OK:
            if (btn_ev == BTN_EVENT_LONG_PRESS) {
                SWITCH_TO_PD_SELECT();
            } else {
                if (pd_verify_adc(m_cur_pd) == FALSE) {
                    DO_PD_CFG_FAIL();
                }
            }
            break;

        case SYS_STATE_VERIFY_FAIL:
            if (btn_ev == BTN_EVENT_LONG_PRESS) {
                SWITCH_TO_PD_SELECT();
            }
            break;
        }

        leds_process();

        mDelaymS(PERIOD_MS);
    }
}
